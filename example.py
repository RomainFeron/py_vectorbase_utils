from py_vectorbase_utils import VectorBaseUtils

# List of species for which to download the genome
species_list = ['aedes_aegypti',
                'aedes_albopictus',
                'anopheles_albimanus',
                'anopheles_arabiensis',
                'anopheles_atroparvus',
                'anopheles_christyi',
                'anopheles_coluzzii',
                'anopheles_culicifacies',
                'anopheles_darlingi',
                'anopheles_dirus',
                'anopheles_epiroticus',
                'anopheles_farauti',
                'anopheles_funestus',
                'anopheles_gambiae',
                'anopheles_maculatus',
                'anopheles_melas',
                'anopheles_merus',
                'anopheles_merus',
                'anopheles_minimus',
                'anopheles_minimus',
                'anopheles_quadriannulatus',
                'anopheles_quadriannulatus',
                'anopheles_sinensis',
                'anopheles_sinensis',
                'anopheles_stephensi',
                'anopheles_stephensi',
                'culex_quinquefasciatus']

# Initialize the VectorBaseUtils instance
vb_utils = VectorBaseUtils()

# Download a single genome from the list
vb_utils.genome_downloader.download_genome(species_list[0], output_dir='genomes/')

# Download all genomes from the list
# Genome downloader parameters can also be set for the instance as follows:
vb_utils.genome_downloader.output_dir = 'genomes/'
vb_utils.genome_downloader.decompress = True
for species in species_list:
    vb_utils.genome_downloader.download_genome(species)
