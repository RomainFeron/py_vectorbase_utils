# py_vectorbase_utils

Collection of utilities to use VectorBase with python.
Currently implemented utilities:
  - Genome downloader: download the latest assembly from a species name (`<genus>_<species>`)

## Installation

### Get the latest release with pip

```bash
pip3 install py-vectorbase-utils
```

### Get the latest version from GitLab

```bash
git clone git@gitlab.com:RomainFeron/py_vectorbase_utils.git
sudo python3 setup.py install
```

## Usage

A minimal example is provided in `example.py`.
